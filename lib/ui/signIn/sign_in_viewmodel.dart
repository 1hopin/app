import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/injection.dart';
import 'package:onehopin/models/otp/customer_otp_request.dart';
import 'package:onehopin/models/otp/otp_resend.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/signIn/signin.dart';
import 'package:onehopin/service/customer_otp_request_service.dart';
import 'package:onehopin/service/otp_resend_service.dart';
import 'package:onehopin/service/signin_service.dart';

@injectable
class SignInViewModel extends ChangeNotifier {
  Resource<CustomerOtpRequest?>? _customerotpdata;
  Resource<CustomerOtpRequest?>? get customerotpdata => _customerotpdata;
  Resource<OtpResend?>? _otpresend;
  Resource<OtpResend?>? get otpresend => _otpresend;
  Resource<Signin?>? _signincustomer;
  Resource<Signin?>? get signincustomer => _signincustomer;
  String? refnootp = "";

  Future<Resource<CustomerOtpRequest?>?> getCustomerOtp(String? phoneno) async {
    Resource<CustomerOtpRequest?>? result;
    result = await getIt<CustomerOtpRequestService>().getCustomerOtp(phoneno);
    _customerotpdata = result;
    return _customerotpdata;
  }

  Future<Resource<OtpResend?>?> getOtpResend(String? phoneno) async {
    Resource<OtpResend?>? result;
    result = await getIt<OtpResendService>().getOtpResend(phoneno);
    _otpresend = result;
    return _otpresend;
  }

  Future<Resource<Signin?>?> signInCustomer(
      String? token, String? pin, String? phoneno) async {
    Resource<Signin?>? result;
    result = await getIt<SigninService>().signInCustomer(token, pin, phoneno);
    _signincustomer = result;
    return _signincustomer;
  }
}
