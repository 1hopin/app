import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onehopin/constants/app_assets.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_constants.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signIn/sign_in_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignInPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _controllersc = TextEditingController();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
          backgroundColor: mainOneHopIn,
          body: SafeArea(
            child: SingleChildScrollView(
              reverse: true,
              child: Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 45.w, 0, 0),
                    child: Column(
                      children: [
                        Container(
                          width: 120,
                          height: 120,
                          child: Image.asset(AppAssets.iconOneHopIn),
                        ),
                        constSpacing10,
                        Container(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(40, 0, 10, 0),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 35,
                                  height: 35,
                                  child: Image.asset(AppAssets.flagTh),
                                ),
                                const SizedBox(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(3, 0, 10, 0),
                                    child: Text("+66"),
                                  ),
                                ),
                                Container(
                                  width: 58.w,
                                  height: 5.h,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: mainOneHopIn,
                                    boxShadow: const [
                                      BoxShadow(color: gray_1, spreadRadius: 1),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: TextField(
                                      controller: _controllersc,
                                      maxLines: 1,
                                      keyboardType: TextInputType.phone,
                                      decoration: InputDecoration.collapsed(
                                        hintText: tr('signin.numberInput'),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        constSpacing11,
                        Container(
                          width: 80.w,
                          height: 50,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          primary_4),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                  ))),
                              onPressed: () {
                                if (_controllersc.text.isNotEmpty) {
                                  if (_controllersc.text.length == 10) {
                                    // FocusScope.of(context).unfocus();
                                    EasyLoading.show(
                                      status: tr('loading'),
                                      maskType: EasyLoadingMaskType.black,
                                    );
                                    context
                                        .read<SignInViewModel>()
                                        .getCustomerOtp(_controllersc.text)
                                        .then((value) => {
                                              if (value?.status ==
                                                  Status.SUCCESS)
                                                {
                                                  EasyLoading.dismiss(),
                                                  context
                                                          .read<SignInViewModel>()
                                                          .refnootp =
                                                      value?.data?.otp?.refno,
                                                  context.router.push(
                                                      RequestOtpRoute(
                                                          phoneno: _controllersc
                                                              .text)),
                                                }
                                              else
                                                {
                                                  EasyLoading.showError(
                                                      "ไม่สามารถดำเนินการได้",
                                                      duration: const Duration(
                                                          milliseconds: 400)),
                                                }
                                            });
                                  } else {
                                    EasyLoading.showError(
                                        "รูปแบบหมายเลขโทรศัพท์ไม่ถูกต้อง",
                                        duration:
                                            const Duration(milliseconds: 400));
                                  }
                                } else {
                                  EasyLoading.showInfo(
                                      "กรุณาระบุหมายเลขโทรศัพท์",
                                      duration: Duration(milliseconds: 400));
                                }
                              },
                              child: Text(tr('signin.submit'),
                                  style: TextStyle(fontSize: 16))),
                        ),
                        constSpacing4,
                        // Container(
                        //   child: Row(
                        //     mainAxisAlignment: MainAxisAlignment.center,
                        //     crossAxisAlignment: CrossAxisAlignment.center,
                        //     children: [
                        //       Text(tr('signin.title_1')),
                        //       Text(" "),
                        //       GestureDetector(
                        //         onTap: () {
                        //           context.router.push(SignUpRoute());
                        //         },
                        //         child: Text(
                        //           tr('signin.title_2'),
                        //           style: const TextStyle(
                        //             color: visited,
                        //             fontSize: 14.0,
                        //             decoration: TextDecoration.underline,
                        //           ),
                        //         ),
                        //       )
                        //     ],
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
