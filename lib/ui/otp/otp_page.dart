import 'package:flutter/material.dart';
import 'package:onehopin/constants/app_colors.dart';

class OtpPage extends StatefulWidget {
  const OtpPage({Key? key}) : super(key: key);

  @override
  _OtpState createState() => _OtpState();
}

class _OtpState extends State<OtpPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainOneHopIn,
      body: SafeArea(
        child: Container(
          child: Align(
            alignment: Alignment.center,
            child: Text("OTP"),
          ),
        ),
      ),
    );
  }
}
