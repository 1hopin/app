import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onehopin/constants/app_assets.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_constants.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:sizer/sizer.dart';

class VerifyingPage extends StatefulWidget {
  const VerifyingPage({Key? key}) : super(key: key);

  @override
  _VerifyingPageState createState() => _VerifyingPageState();
}

class _VerifyingPageState extends State<VerifyingPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 2), () {
      context.router.push(HomeRoute());
    });

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
          backgroundColor: mainOneHopIn,
          body: SafeArea(
            child: SingleChildScrollView(
              reverse: true,
              child: Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 50.w, 0, 0),
                    child: Column(
                      children: [
                        Container(
                          width: 120,
                          height: 120,
                          child: Image.asset(AppAssets.iconOneHopIn),
                        ),
                        constSpacing11,
                        Container(
                          child: Text(
                            tr('verifying.title_1'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
