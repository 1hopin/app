import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/constants/app_key.dart';
import 'package:onehopin/injection.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/signUp/signup.dart';
import 'package:onehopin/service/signup_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

@injectable
class SignUpViewModel extends ChangeNotifier {
  Resource<Signup?>? _signup;
  Resource<Signup?>? get signup => _signup;

  Future<Resource<Signup?>?> signupCustomer(
      String? phone, String? name, String? lastname) async {
    Resource<Signup?>? result;
    result = await getIt<SignupService>().signupCustomer(phone, name, lastname);
    _signup = result;
    return _signup;
  }

  Future<void> setTokenCustome(String? token) async {
    final prefs = await SharedPreferences.getInstance();
    if (token != null) {
      if (token.isNotEmpty) {
        prefs.setString(tokenCustomer, token);
      }
    }
  }

  Future<void> clearTokenCustome() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(tokenCustomer);
  }

  Future<String?> getTokenCustome() async {
    String? result;
    final prefs = await SharedPreferences.getInstance();
    result = prefs.getString(tokenCustomer);
    return result;
  }
}
