import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onehopin/constants/app_assets.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_constants.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signUp/sign_up_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key, required this.phoneno}) : super(key: key);
  final String phoneno;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _controllerscFn = TextEditingController();
    final _controllerscLn = TextEditingController();

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: mainOneHopIn,
        body: SafeArea(
          child: SingleChildScrollView(
            reverse: true,
            child: Container(
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 35.w, 0, 0),
                  child: Column(
                    children: [
                      Container(
                        width: 120,
                        height: 120,
                        child: Image.asset(AppAssets.iconOneHopIn),
                      ),
                      constSpacing6,
                      Container(
                        child: Text(
                          tr('signup.title_1'),
                          style: TextStyle(fontSize: 28),
                        ),
                      ),
                      constSpacing4,
                      Container(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Column(
                            children: [
                              // SizedBox(
                              //   width: 79.w,
                              //   height: 4.h,
                              //   child: Align(
                              //     alignment: Alignment.centerRight,
                              //     child: Text(
                              //       tr('signup.title_2'),
                              //       style: const TextStyle(color: gray_m_1),
                              //     ),
                              //   ),
                              // ),
                              Container(
                                width: 80.w,
                                height: 5.h,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: mainOneHopIn,
                                  boxShadow: const [
                                    BoxShadow(color: gray_1, spreadRadius: 1),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(6),
                                  child: TextField(
                                    controller: _controllerscFn,
                                    maxLines: 1,
                                    keyboardType: TextInputType.text,
                                    // inputFormatters: <TextInputFormatter>[
                                    //   FilteringTextInputFormatter.allow(
                                    //       RegExp("[a-zA-Z0-9 ]"))
                                    // ],
                                    decoration: InputDecoration.collapsed(
                                      hintText: tr('signup.title_3'),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      constSpacing3,
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Container(
                            width: 80.w,
                            height: 5.h,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: mainOneHopIn,
                              boxShadow: const [
                                BoxShadow(color: gray_1, spreadRadius: 1),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(6),
                              child: TextField(
                                controller: _controllerscLn,
                                maxLines: 1,
                                keyboardType: TextInputType.text,
                                // inputFormatters: <TextInputFormatter>[
                                //   FilteringTextInputFormatter.allow(
                                //       RegExp("[a-zA-Z0-9 ]"))
                                // ],
                                decoration: InputDecoration.collapsed(
                                  hintText: tr('signup.title_4'),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      constSpacing4,
                      Container(
                        width: 80.w,
                        height: 50,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.white),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(primary_4),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                ))),
                            onPressed: () {
                              if (_controllerscFn.text.isEmpty) {
                                EasyLoading.showInfo(tr('signup.empty_name'),
                                    duration:
                                        const Duration(milliseconds: 400));
                              } else if (_controllerscLn.text.isEmpty) {
                                EasyLoading.showInfo(
                                    tr('signup.empty_lastname'),
                                    duration:
                                        const Duration(milliseconds: 400));
                              } else {
                                EasyLoading.show(
                                  status: tr('loading'),
                                  maskType: EasyLoadingMaskType.black,
                                );
                                context
                                    .read<SignUpViewModel>()
                                    .signupCustomer(
                                        widget.phoneno,
                                        _controllerscFn.text,
                                        _controllerscLn.text)
                                    .then((redCheckOtp) {
                                  if (redCheckOtp?.status == Status.SUCCESS) {
                                    EasyLoading.dismiss();
                                    context
                                        .read<SignUpViewModel>()
                                        .setTokenCustome(
                                            redCheckOtp?.data?.token);
                                    context.router.push(HomeRoute());
                                  } else {
                                    EasyLoading.dismiss();
                                    if (redCheckOtp?.message == "is_existed") {
                                      EasyLoading.showError(
                                          tr('signup.is_existed'),
                                          duration: const Duration(
                                              milliseconds: 400));
                                    } else {
                                      EasyLoading.showError(tr('api.error'),
                                          duration: const Duration(
                                              milliseconds: 400));
                                    }
                                  }
                                });
                              }
                            },
                            child: Text(tr('signup.submit'),
                                style: TextStyle(fontSize: 16))),
                      ),
                      constSpacing4,
                      Visibility(
                        visible: false,
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(tr('signup.title_5')),
                              Text(" "),
                              GestureDetector(
                                onTap: () {
                                  context.router.push(SignInRoute());
                                },
                                child: Text(
                                  tr('signup.title_6'),
                                  style: const TextStyle(
                                    color: visited,
                                    fontSize: 14.0,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
