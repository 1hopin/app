import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/injection.dart';
import 'package:onehopin/models/customersProfile/customers_profile.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/customer_profile_service.dart';

@injectable
class HomeViewModel extends ChangeNotifier {
  Resource<CustomersProfile?>? _customersprofile;
  Resource<CustomersProfile?>? get customersprofile => _customersprofile;

  Future<void> getCustomerProfile() async {
    Resource<CustomersProfile?>? result;
    result = await getIt<CustomerProfileService>().getProfileCustomer();
    _customersprofile = result;
    notifyListeners();
  }
}
