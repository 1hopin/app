import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/models/customersProfile/customers_profile.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/ui/home/home_viewmodel.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signUp/sign_up_viewmodel.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    context.read<HomeViewModel>().getCustomerProfile();
  }

  @override
  Widget build(BuildContext context) {
    Resource<CustomersProfile?>? customersprofiledata =
        context.watch<HomeViewModel>().customersprofile;
    if (customersprofiledata?.message == "unauthorized") {
      context.read<SignUpViewModel>().clearTokenCustome();
      Future.delayed(const Duration(milliseconds: 500), () {
        context.router.push(SignInRoute());
      });
    }

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: mainOneHopIn,
        appBar: AppBar(
          title: Text("Welcome"),
          automaticallyImplyLeading: false,
        ),
        body: SafeArea(
          child: Container(
            child: Align(
              alignment: Alignment.center,
              child: Column(children: [
                Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                    child: Column(
                      children: [
                        Text("ID : ${customersprofiledata?.data?.id ?? ''}"),
                        Text(
                            "ชื่อ : ${customersprofiledata?.data?.firstname ?? ''}"),
                        Text(
                            "นามสกุล : ${customersprofiledata?.data?.lastname ?? ''}"),
                        Text(
                            "เบอร์โทร์ : ${customersprofiledata?.data?.phoneNo ?? ''}"),
                      ],
                    ),
                  ),
                ),
                ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(primary_4),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ))),
                    onPressed: () {
                      context.read<SignUpViewModel>().clearTokenCustome();
                      Future.delayed(const Duration(milliseconds: 500), () {
                        context.router.push(SignInRoute());
                      });
                    },
                    child: Text(tr('signout.submit'),
                        style: TextStyle(fontSize: 16)))
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
