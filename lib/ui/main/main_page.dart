import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signUp/sign_up_viewmodel.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: mainOneHopIn,
        body: SafeArea(
          child: Container(
            child: Align(
              alignment: Alignment.center,
              child: FutureBuilder(
                future: context.read<SignUpViewModel>().getTokenCustome(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data != null) {
                    context.router.push(HomeRoute());
                  } else {
                    context.router.push(SignInRoute());
                  }
                  return const Text("1Hopin.....");
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
