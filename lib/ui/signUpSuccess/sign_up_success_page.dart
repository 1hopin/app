import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onehopin/constants/app_assets.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_constants.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:sizer/sizer.dart';

class SignUpSuccessPage extends StatefulWidget {
  const SignUpSuccessPage({Key? key}) : super(key: key);

  @override
  _SignUpSuccessPageState createState() => _SignUpSuccessPageState();
}

class _SignUpSuccessPageState extends State<SignUpSuccessPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: mainOneHopIn,
        body: SafeArea(
          child: SingleChildScrollView(
            reverse: true,
            child: Container(
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 50.w, 0, 0),
                  child: Column(
                    children: [
                      Container(
                        width: 120,
                        height: 120,
                        child: Image.asset(AppAssets.iconOneHopIn),
                      ),
                      constSpacing11,
                      Container(
                        child: const Text(
                          "0839805024",
                          style: TextStyle(color: positive_1),
                        ),
                      ),
                      Container(
                        child: Text(
                          tr('signupSuccess.title_1'),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(tr('signupSuccess.title_2')),
                            Text(" "),
                            GestureDetector(
                              onTap: () {
                                context.router.push(const SignInRoute());
                              },
                              child: Text(
                                tr('signupSuccess.title_3'),
                                style: const TextStyle(
                                  color: visited,
                                  fontSize: 14.0,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                            Text(" "),
                            Text(tr('signupSuccess.title_4')),
                          ],
                        ),
                      ),
                      constSpacing4,
                      Container(
                        width: 80.w,
                        height: 50,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.white),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(primary_4),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                ))),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              context.router.push(SignInRoute());
                            },
                            child: Text(tr('signin.submit'),
                                style: TextStyle(fontSize: 16))),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
