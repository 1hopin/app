// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i10;
import 'package:flutter/material.dart' as _i11;

import '../home/home_page.dart' as _i6;
import '../invalidAccount/invalid_account_page.dart' as _i4;
import '../main/main_page.dart' as _i1;
import '../otp/otp_page.dart' as _i7;
import '../requestOTP/request_otp_page.dart' as _i9;
import '../signIn/sign_in_page.dart' as _i2;
import '../signUp/sign_up_page.dart' as _i3;
import '../signUpSuccess/sign_up_success_page.dart' as _i5;
import '../verifying/verifying_page.dart' as _i8;

class AppRouter extends _i10.RootStackRouter {
  AppRouter([_i11.GlobalKey<_i11.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i10.PageFactory> pagesMap = {
    MainRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.MainPage());
    },
    SignInRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.SignInPage());
    },
    SignUpRoute.name: (routeData) {
      final args = routeData.argsAs<SignUpRouteArgs>();
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i3.SignUpPage(key: args.key, phoneno: args.phoneno));
    },
    InvalidAccountRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.InvalidAccountPage());
    },
    SignUpSuccessRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.SignUpSuccessPage());
    },
    HomeRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i6.HomePage());
    },
    OtpRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i7.OtpPage());
    },
    VerifyingRoute.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i8.VerifyingPage());
    },
    RequestOtpRoute.name: (routeData) {
      final args = routeData.argsAs<RequestOtpRouteArgs>();
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i9.RequestOtpPage(key: args.key, phoneno: args.phoneno));
    }
  };

  @override
  List<_i10.RouteConfig> get routes => [
        _i10.RouteConfig(MainRoute.name, path: '/'),
        _i10.RouteConfig(SignInRoute.name, path: '/sign-in-page'),
        _i10.RouteConfig(SignUpRoute.name, path: '/sign-up-page'),
        _i10.RouteConfig(SignUpRoute.name, path: '/sign-up-page'),
        _i10.RouteConfig(InvalidAccountRoute.name,
            path: '/invalid-account-page'),
        _i10.RouteConfig(SignUpSuccessRoute.name,
            path: '/sign-up-success-page'),
        _i10.RouteConfig(HomeRoute.name, path: '/home-page'),
        _i10.RouteConfig(OtpRoute.name, path: '/otp-page'),
        _i10.RouteConfig(VerifyingRoute.name, path: '/verifying-page'),
        _i10.RouteConfig(RequestOtpRoute.name, path: '/request-otp-page')
      ];
}

/// generated route for
/// [_i1.MainPage]
class MainRoute extends _i10.PageRouteInfo<void> {
  const MainRoute() : super(MainRoute.name, path: '/');

  static const String name = 'MainRoute';
}

/// generated route for
/// [_i2.SignInPage]
class SignInRoute extends _i10.PageRouteInfo<void> {
  const SignInRoute() : super(SignInRoute.name, path: '/sign-in-page');

  static const String name = 'SignInRoute';
}

/// generated route for
/// [_i3.SignUpPage]
class SignUpRoute extends _i10.PageRouteInfo<SignUpRouteArgs> {
  SignUpRoute({_i11.Key? key, required String phoneno})
      : super(SignUpRoute.name,
            path: '/sign-up-page',
            args: SignUpRouteArgs(key: key, phoneno: phoneno));

  static const String name = 'SignUpRoute';
}

class SignUpRouteArgs {
  const SignUpRouteArgs({this.key, required this.phoneno});

  final _i11.Key? key;

  final String phoneno;

  @override
  String toString() {
    return 'SignUpRouteArgs{key: $key, phoneno: $phoneno}';
  }
}

/// generated route for
/// [_i4.InvalidAccountPage]
class InvalidAccountRoute extends _i10.PageRouteInfo<void> {
  const InvalidAccountRoute()
      : super(InvalidAccountRoute.name, path: '/invalid-account-page');

  static const String name = 'InvalidAccountRoute';
}

/// generated route for
/// [_i5.SignUpSuccessPage]
class SignUpSuccessRoute extends _i10.PageRouteInfo<void> {
  const SignUpSuccessRoute()
      : super(SignUpSuccessRoute.name, path: '/sign-up-success-page');

  static const String name = 'SignUpSuccessRoute';
}

/// generated route for
/// [_i6.HomePage]
class HomeRoute extends _i10.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: '/home-page');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i7.OtpPage]
class OtpRoute extends _i10.PageRouteInfo<void> {
  const OtpRoute() : super(OtpRoute.name, path: '/otp-page');

  static const String name = 'OtpRoute';
}

/// generated route for
/// [_i8.VerifyingPage]
class VerifyingRoute extends _i10.PageRouteInfo<void> {
  const VerifyingRoute() : super(VerifyingRoute.name, path: '/verifying-page');

  static const String name = 'VerifyingRoute';
}

/// generated route for
/// [_i9.RequestOtpPage]
class RequestOtpRoute extends _i10.PageRouteInfo<RequestOtpRouteArgs> {
  RequestOtpRoute({_i11.Key? key, required String phoneno})
      : super(RequestOtpRoute.name,
            path: '/request-otp-page',
            args: RequestOtpRouteArgs(key: key, phoneno: phoneno));

  static const String name = 'RequestOtpRoute';
}

class RequestOtpRouteArgs {
  const RequestOtpRouteArgs({this.key, required this.phoneno});

  final _i11.Key? key;

  final String phoneno;

  @override
  String toString() {
    return 'RequestOtpRouteArgs{key: $key, phoneno: $phoneno}';
  }
}
