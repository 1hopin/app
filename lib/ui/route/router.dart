import 'package:auto_route/auto_route.dart';
import 'package:onehopin/ui/home/home_page.dart';
import 'package:onehopin/ui/invalidAccount/invalid_account_page.dart';
import 'package:onehopin/ui/main/main_page.dart';
import 'package:onehopin/ui/otp/otp_page.dart';
import 'package:onehopin/ui/requestOTP/request_otp_page.dart';
import 'package:onehopin/ui/signIn/sign_in_page.dart';
import 'package:onehopin/ui/signUp/sign_up_page.dart';
import 'package:onehopin/ui/signUpSuccess/sign_up_success_page.dart';
import 'package:onehopin/ui/verifying/verifying_page.dart';

@MaterialAutoRouter(replaceInRouteName: 'Page,Route', routes: <AutoRoute>[
  AutoRoute(page: MainPage, initial: true),
  AutoRoute(page: SignInPage),
  AutoRoute(page: SignUpPage),
  AutoRoute(page: SignUpPage),
  AutoRoute(page: InvalidAccountPage),
  AutoRoute(page: SignUpSuccessPage),
  AutoRoute(page: HomePage),
  AutoRoute(page: OtpPage),
  AutoRoute(page: VerifyingPage),
  AutoRoute(page: RequestOtpPage),
])
class $AppRouter {}
