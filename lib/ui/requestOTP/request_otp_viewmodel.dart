import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/injection.dart';
import 'package:onehopin/models/otp/otp_verify.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/otp_verify_service.dart';

@injectable
class RequestOtpViewmodel extends ChangeNotifier {
  Resource<OtpVerify?>? _otpverify;
  Resource<OtpVerify?>? get otpverify => _otpverify;

  Future<Resource<OtpVerify?>?> chekOtpVerify(
      String? token, String? pin) async {
    Resource<OtpVerify?>? result;
    result = await getIt<OtpVerifyService>().checkOtpVerify(token, pin);
    _otpverify = result;
    return _otpverify;
  }
}
