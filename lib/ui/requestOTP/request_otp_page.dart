import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onehopin/constants/app_assets.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_constants.dart';
import 'package:onehopin/models/otp/customer_otp_request.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/ui/requestOTP/request_otp_viewmodel.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signIn/sign_in_viewmodel.dart';
import 'package:onehopin/ui/signUp/sign_up_viewmodel.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class RequestOtpPage extends StatefulWidget {
  const RequestOtpPage({Key? key, required this.phoneno}) : super(key: key);
  final String phoneno;

  @override
  _RequestOtpPageState createState() => _RequestOtpPageState();
}

class _RequestOtpPageState extends State<RequestOtpPage> {
  late CountdownTimerController controller;
  int endTime = DateTime.now().millisecondsSinceEpoch;
  bool waitResendOtp = false;
  final pinController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  void onEnd() {
    waitResendOtp = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 40,
      height: 50,
      textStyle: const TextStyle(
          fontSize: 20,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.normal),
      decoration: BoxDecoration(
        border: Border.all(color: gray_2),
        borderRadius: BorderRadius.circular(1),
      ),
    );

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: mainOneHopIn,
        body: SafeArea(
          child: SingleChildScrollView(
            reverse: true,
            child: Container(
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 40.w, 0, 0),
                  child: Column(
                    children: [
                      Container(
                        width: 120,
                        height: 120,
                        child: Image.asset(AppAssets.iconOneHopIn),
                      ),
                      constSpacing6,
                      Container(
                        child: Text(
                          tr('requestotp.title_1'),
                          style: TextStyle(fontSize: 28),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Text(
                                tr('requestotp.title_2'),
                              ),
                            ),
                            Container(
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  child: Text(widget.phoneno)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Text(
                                tr('requestotp.title_3'),
                              ),
                            ),
                            Container(
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  child: Text(context
                                          .read<SignInViewModel>()
                                          .refnootp ??
                                      "")),
                            ),
                          ],
                        ),
                      ),
                      constSpacing6,
                      Container(
                          child: Pinput(
                        controller: pinController,
                        defaultPinTheme: defaultPinTheme,
                        length: 6,
                      )),
                      constSpacing6,
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            if (!waitResendOtp) ...[
                              GestureDetector(
                                onTap: () {
                                  EasyLoading.show(
                                    status: tr('loading'),
                                    maskType: EasyLoadingMaskType.black,
                                  );
                                  context
                                      .read<SignInViewModel>()
                                      .getOtpResend(widget.phoneno)
                                      .then((value) => {
                                            if (value?.status == Status.SUCCESS)
                                              {
                                                EasyLoading.dismiss(),
                                                context
                                                        .read<SignInViewModel>()
                                                        .refnootp =
                                                    value?.data?.otp?.refno,
                                              }
                                            else
                                              {
                                                EasyLoading.dismiss(),
                                                EasyLoading.showError(
                                                    "ไม่สามารถดำเนินการได้",
                                                    duration: const Duration(
                                                        milliseconds: 400)),
                                              }
                                          });

                                  controller = CountdownTimerController(
                                      endTime: DateTime.now()
                                              .millisecondsSinceEpoch +
                                          1000 * 60,
                                      onEnd: onEnd);
                                  waitResendOtp = true;
                                  pinController.clear();
                                  setState(() {});
                                },
                                child: Text(
                                  tr('requestotp.title_4'),
                                  style: const TextStyle(
                                    color: visited,
                                    fontSize: 14.0,
                                  ),
                                ),
                              ),
                            ] else ...[
                              const Icon(
                                Icons.cached,
                                color: positive_1,
                              ),
                              const Text(" "),
                              Text(
                                tr('requestotp.title_5'),
                                style: const TextStyle(
                                  color: positive_1,
                                  fontSize: 14.0,
                                ),
                              ),
                              const Text(" "),
                              CountdownTimer(
                                controller: controller,
                                widgetBuilder: (context, time) {
                                  return Text(
                                    "${time?.min ?? '00'}:${time?.sec ?? '00'}",
                                    style: const TextStyle(
                                      color: positive_1,
                                      fontSize: 14.0,
                                    ),
                                  );
                                },
                              ),
                              const Text(" "),
                              Text(
                                tr('requestotp.title_6'),
                                style: const TextStyle(
                                  color: positive_1,
                                  fontSize: 14.0,
                                ),
                              ),
                            ]
                          ],
                        ),
                      ),
                      constSpacing3,
                      Container(
                        width: 80.w,
                        height: 50,
                        child: ElevatedButton(
                            style: ButtonStyle(
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.white),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(primary_4),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                ))),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              if (pinController.text.isNotEmpty) {
                                Resource<CustomerOtpRequest?>? customerotpdata =
                                    context
                                        .read<SignInViewModel>()
                                        .customerotpdata;
                                if (customerotpdata?.status == Status.SUCCESS) {
                                  EasyLoading.show(
                                    status: tr('loading'),
                                    maskType: EasyLoadingMaskType.black,
                                  );
                                  if (customerotpdata?.data?.type?.isType ==
                                      "old") {
                                    // context.router.push(VerifyingRoute());
                                    context
                                        .read<SignInViewModel>()
                                        .signInCustomer(
                                            customerotpdata?.data?.otp?.token,
                                            pinController.text,
                                            widget.phoneno)
                                        .then((resSignIn) {
                                      if (resSignIn?.status == Status.SUCCESS) {
                                        EasyLoading.dismiss();
                                        context
                                            .read<SignUpViewModel>()
                                            .setTokenCustome(
                                                resSignIn?.data?.token);
                                        context.router.push(VerifyingRoute());
                                      } else {
                                        EasyLoading.dismiss();
                                        EasyLoading.showError(tr('api.error'),
                                            duration: const Duration(
                                                milliseconds: 400));
                                      }
                                    });
                                  } else {
                                    context
                                        .read<RequestOtpViewmodel>()
                                        .chekOtpVerify(
                                            customerotpdata?.data?.otp?.token,
                                            pinController.text)
                                        .then((resCheckOtp) {
                                      if (resCheckOtp?.status ==
                                          Status.SUCCESS) {
                                        EasyLoading.dismiss();
                                        context.router.push(SignUpRoute(
                                            phoneno: widget.phoneno));
                                      } else {
                                        EasyLoading.dismiss();
                                        EasyLoading.showError(
                                            tr('otp.incorrect'),
                                            duration: const Duration(
                                                milliseconds: 400));
                                      }
                                    });
                                  }
                                } else {
                                  EasyLoading.showError(tr('api.error'),
                                      duration:
                                          const Duration(milliseconds: 400));
                                }
                              } else {
                                EasyLoading.showInfo(tr('otp.empty'),
                                    duration:
                                        const Duration(milliseconds: 400));
                              }
                            },
                            child: Text(tr('requestotp.submit'),
                                style: TextStyle(fontSize: 16))),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
