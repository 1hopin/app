import 'package:onehopin/app_config.dart';
import 'package:onehopin/environment_config.dart';
import 'package:onehopin/main.dart';

Future<void> main() async {
  AppConfig devAppConfig = AppConfig(flavorName: Environments.uat);
  EnvironmentConfig.getInstance(flavorName: devAppConfig.flavorName);
  mainApp();
}
