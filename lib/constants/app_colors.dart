import 'package:flutter/material.dart';

//Main
const Color primary_1 = Color(0xff181C24);
const Color primary_2 = Color(0xff93743D);
const Color primary_3 = Color(0xffF5CC5A);
const Color primary_4 = Color(0xffFF6200);

//Secondary
const Color mainOneHopIn = Color.fromRGBO(255, 255, 255, 1);
const Color gray_1 = Color(0xffE5E5E7);
const Color gray_2 = Color(0xffDEDEDE);
const Color gray_m_1 = Color(0xff91959C);
const Color gray_d_1 = Color(0xff535965);
const Color gray_d_2 = Color(0xff181C24);

//Semantic
const Color positive_1 = Color(0xff00A075);
const Color positive_2 = Color(0xff007A59);
const Color negative_1 = Color(0xffE84E55);
const Color negative_2 = Color(0xffCC0E17);
const Color inactive_1 = Color(0xffEFF0F0);
const Color inactive_2 = Color(0xff9195C);
const Color visited = Color(0xff5E8BFC);
const Color link = Color(0xff009EB9);
