class AppAssets {
  static const String logoOneHopIn = 'assets/images/1hopin_logo.png';
  static const String iconOneHopIn = 'assets/images/1hopin_icon.png';
  //Flag
  static const String flagTh = 'assets/images/flag/flag-th.png';
}
