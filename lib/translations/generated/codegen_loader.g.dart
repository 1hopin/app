// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "language": "English",
  "nameapp": "1Hopin",
  "loading": "please wait a moment.....",
  "api": {
    "error": "Unable to perform"
  },
  "signin": {
    "numberInput": "Phone number",
    "submit": "Sign in",
    "title_1": "Don't have an account?",
    "title_2": "Register"
  },
  "signout": {
    "submit": "Sign Out"
  },
  "signup": {
    "submit": "Sign up",
    "title_1": "Sign up",
    "title_2": "English only",
    "title_3": "First name",
    "title_4": "Last name",
    "title_5": "Already have an account?",
    "title_6": "Sign in",
    "empty_name": "Please enter your name",
    "empty_lastname": "Please enter your last name",
    "is_existed": "This user is already in the system."
  },
  "signupSuccess": {
    "submit": "Sign up",
    "title_1": "Has been registered",
    "title_2": "You can",
    "title_3": "Sign up",
    "title_4": "with this phone number"
  },
  "verifying": {
    "title_1": "Successfully verified Logging in"
  },
  "invalidAccount": {
    "submit": "Sign up",
    "title_1": "The system cannot find the number.",
    "title_2": "You can",
    "title_3": "Sign up",
    "title_4": "easily"
  },
  "requestotp": {
    "submit": "Confirm",
    "title_1": "Enter verification code",
    "title_2": "The system has sent an OTP to",
    "title_3": "reference",
    "title_4": "Resend OTP",
    "title_5": "Please wait",
    "title_6": "minutes to send OTP again"
  },
  "otp": {
    "empty": "Please specify OTP",
    "incorrect": "Invalid OTP"
  }
};
static const Map<String,dynamic> th = {
  "language": "ภาษาไทย",
  "nameapp": "1Hopin",
  "loading": "กรุณารอสักครู่.....",
  "api": {
    "error": "ไม่สามารถดำเนินการได้"
  },
  "signin": {
    "numberInput": "หมายเลขโทรศัพท์",
    "submit": "เข้าสู่ระบบ",
    "title_1": "ยังไม่มีบัญชี?",
    "title_2": "ลงทะเบียน"
  },
  "signout": {
    "submit": "ออกจากระบบ"
  },
  "signup": {
    "submit": "ลงทะเบียน",
    "title_1": "ลงทะเบียน",
    "title_2": "ภาษาอังกฤษเท่านั้น",
    "title_3": "ชื่อ",
    "title_4": "นามสกุล",
    "title_5": "มีบัญชีแล้ว?",
    "title_6": "เข้าสู่ระบบ",
    "empty_name": "กรุณาระบุชื่อ",
    "empty_lastname": "กรุณาระบุนามสกุล",
    "is_existed": "ผู้ใช้งานนี้มีในระบบแล้ว"
  },
  "signupSuccess": {
    "submit": "เข้าสู่ระบบ",
    "title_1": "ได้ทำการลงลงทะเบียนแล้ว",
    "title_2": "คุณสามารถ",
    "title_3": "เข้าสู่ระบบ",
    "title_4": "ได้ด้วยหมายเลขโทรศัพท์นี้"
  },
  "verifying": {
    "title_1": "ยืนยันตัวตนสำเร็จ กำลังเข้าสู่ระบบ"
  },
  "invalidAccount": {
    "submit": "ลงทะเบียน",
    "title_1": "ระบบไม่พบหมายเลข",
    "title_2": "คุณสามารถ",
    "title_3": "ลงทะเบียน",
    "title_4": "ได้ง่ายๆ"
  },
  "requestotp": {
    "submit": "ยืนยัน",
    "title_1": "กรอกรหัสยืนยัน",
    "title_2": "ระบบได้จัดส่ง OTP ไปยัง",
    "title_3": "อ้างอิงหมายเลข",
    "title_4": "ส่ง OTP อีกครั้ง",
    "title_5": "กรุณารอ",
    "title_6": "นาทีเพื่อส่ง OTP อีกครั้ง"
  },
  "otp": {
    "empty": "กรุณาระบุ OTP",
    "incorrect": "OTP ไม่ถูกต้อง"
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "th": th};
}
