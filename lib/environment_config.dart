import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvironmentConfig {
  final String flavorName;

  EnvironmentConfig({required this.flavorName});

  static EnvironmentConfig? _instance;

  static EnvironmentConfig getInstance({flavorName}) {
    if (_instance == null) {
      _instance = EnvironmentConfig(flavorName: flavorName);
      return _instance!;
    }
    return _instance!;
  }

  static String get fileName {
    if (_instance?.flavorName == Environments.uat) {
      return '.env.uat';
    } else if (_instance!.flavorName == Environments.prod) {
      return '.env.prod';
    } else {
      return '';
    }
  }

  static String get env {
    return _instance?.flavorName ?? '';
  }

  // get value from file .env.*
  static String get apiUrl {
    return dotenv.env['API_URL'] ?? 'API_URL not found';
  }

  // static String get envUse {
  //   return dotenv.env['Env'] ?? 'Environment not found';
  // }
}

class Environments {
  /// name of the environment
  final String name;

  /// default constructor
  const Environments(this.name);

  /// preset of common env name 'prod'
  static const prod = 'prod';

  /// preset of common env name 'test'
  static const uat = 'uat';
}
