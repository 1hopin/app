import 'package:injectable/injectable.dart';
import 'package:onehopin/base/base.dart';
import 'package:onehopin/constants/app_constants.dart';

@injectable
class LocalizationUtil {
  static final LocalizationUtil _localization = LocalizationUtil._internal();
  String currentLanguage = "";

  factory LocalizationUtil() {
    return _localization;
  }

  LocalizationUtil._internal() {
    Base().getUserLanguage().then((lang) => currentLanguage = lang);
  }

  String getCurrentLanguage() {
    return currentLanguage;
  }

  void updateLanguage(String newLanguage) {
    currentLanguage = newLanguage;
  }

  String localizeText(String? enText, String? thText, String? defaultText) {
    if (currentLanguage.toLowerCase() == langTH.toLowerCase()) {
      return thText ?? defaultText ?? "";
    } else if (currentLanguage.toLowerCase() == langEN.toLowerCase()) {
      return enText ?? defaultText ?? "";
    }
    return defaultText ?? "";
  }
}
