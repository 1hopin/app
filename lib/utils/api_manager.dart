import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:onehopin/base/base.dart';
import 'package:onehopin/environment_config.dart';
import 'package:onehopin/models/core/response_generic_data.dart';
import 'package:retrofit/retrofit.dart';

part 'api_manager.g.dart';

class AppInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      print('onError : ${err.message}');
    }
    super.onError(err, handler);
  }
}

@RestApi()
abstract class ApiManager {
  factory ApiManager(Dio dio) {
    dio.options = BaseOptions(
        baseUrl: EnvironmentConfig.apiUrl,
        receiveTimeout: ApisConfig.receiveTimeout,
        connectTimeout: ApisConfig.connectTimeout,
        contentType: ApisConfig.jsonContentType);
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      Base().getTokenHeader().then((value) {
        if (value != null) {
          options.headers['Token'] = value;
        }
        handler.next(options);
      });
    }));
    if (kDebugMode) {
      dio.interceptors.add(AppInterceptor());
      dio.interceptors
          .add(LogInterceptor(requestBody: true, responseBody: true));
    }
    return _ApiManager(dio);
  }

  @GET(Apis.kSplashConfig)
  Future<ResponseGenericData> getSplashConfig();

  @POST(Apis.kCustomerOtpsRequest)
  Future<ResponseGenericData> getCustomerOtpsRequest(
      @Body() Map<String, dynamic> body);

  @POST(Apis.kOtpsVarify)
  Future<ResponseGenericData> checkOtpVerify(@Body() Map<String, dynamic> body);

  @POST(Apis.kOtpsResend)
  Future<ResponseGenericData> getOtpResend(@Body() Map<String, dynamic> body);

  @POST(Apis.kSignUpCustomer)
  Future<ResponseGenericData> signupCustomer(@Body() Map<String, dynamic> body);

  @POST(Apis.kSignInCustomer)
  Future<ResponseGenericData> signInCustomer(@Body() Map<String, dynamic> body);

  @GET(Apis.kProfileCustomer)
  Future<ResponseGenericData> getCustomerProfile();
}

class Apis {
  static const String kSplashConfig =
      '/v3/ce356e94-2d5c-4dc6-9ed0-caf108e45e2d';
  static const String kCustomerOtpsRequest = '/customer/v1/otps/request';
  static const String kOtpsVarify = '/sms/v1/otps/verify';
  static const String kSignUpCustomer = '/customer/v1/customers';
  static const String kSignInCustomer = '/customer/v1/signin';
  static const String kProfileCustomer = '/customer/v1/customers/profile';
  static const String kOtpsResend = '/sms/v1/otps/request';
}

class ApisConfig {
  static const String jsonContentType = 'application/json';
  static const int receiveTimeout = 30 * 1000; //30000 ms = 30 sec
  static const int connectTimeout = 30 * 1000; //30000 ms = 30 sec
}
