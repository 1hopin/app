import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onehopin/constants/app_colors.dart';
import 'package:onehopin/constants/app_key.dart';
import 'package:onehopin/environment_config.dart';
import 'package:onehopin/translations/generated/codegen_loader.g.dart';
import 'package:onehopin/ui/home/home_viewmodel.dart';
import 'package:onehopin/ui/invalidAccount/invalid_account_viewmodel.dart';
import 'package:onehopin/ui/main/main_viewmodel.dart';
import 'package:onehopin/ui/otp/otp_viewmodel.dart';
import 'package:onehopin/ui/requestOTP/request_otp_viewmodel.dart';
import 'package:onehopin/ui/route/router.gr.dart';
import 'package:onehopin/ui/signIn/sign_in_viewmodel.dart';
import 'package:onehopin/ui/signUp/sign_up_viewmodel.dart';
import 'package:onehopin/ui/signUpSuccess/sign_up_success_viewmodel.dart';
import 'package:onehopin/ui/verifying/verifying_viewmodel.dart';
import 'package:onehopin/utils/localization_util.dart';
import 'package:onehopin/utils/utils.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';

import 'injection.dart';

Future<void> mainApp() async {
  await dotenv.load(fileName: EnvironmentConfig.fileName);
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  Intl.defaultLocale = 'th';
  configureInjection(EnvironmentConfig.env);
  checkDeviceId();
  LocalizationUtil();
  runApp(EasyLocalization(
      path: 'lib/translations/',
      useOnlyLangCode: true,
      startLocale: const Locale('th'),
      supportedLocales: const [Locale('th'), Locale('en')],
      assetLoader: const CodegenLoader(),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => getIt<MainViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<SignInViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<SignUpViewModel>()),
          ChangeNotifierProvider(
              create: (_) => getIt<SignUpSuccessViewModel>()),
          ChangeNotifierProvider(
              create: (_) => getIt<InvalidAccountViewmodel>()),
          ChangeNotifierProvider(create: (_) => getIt<HomeViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<OtpViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<VerifyingViewModel>()),
          ChangeNotifierProvider(create: (_) => getIt<RequestOtpViewmodel>()),
        ],
        child: MyApp(),
      )));
  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 30)
    ..userInteractions = false
    ..dismissOnTap = false;
}

Future<void> checkDeviceId() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getString(deviceId) == null) {
    prefs.setString(deviceId, Utils().generateDeviceId());
  }
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final AppRouter _appRouter = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp.router(
        theme: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            colorScheme: Theme.of(context).colorScheme.copyWith(
                  primary: primary_1,
                ),
            textSelectionTheme:
                const TextSelectionThemeData(cursorColor: primary_1),
            textTheme: const TextTheme(
              overline: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              caption: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              button: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              bodyText1: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              bodyText2: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline1: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline2: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline3: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline4: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline5: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              headline6: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              subtitle1: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
              subtitle2: TextStyle(
                fontFamily: 'Kanit',
                fontFamilyFallback: ['Helvetica'],
              ),
            )),
        debugShowCheckedModeBanner: false,
        routerDelegate: _appRouter.delegate(),
        routeInformationParser: _appRouter.defaultRouteParser(),
        locale: context.locale,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        builder: EasyLoading.init(),
      );
    });
  }
}
