// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'splash.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Splash _$SplashFromJson(Map<String, dynamic> json) => Splash(
      title: json['title'] as String?,
    );

SplashData _$SplashDataFromJson(Map<String, dynamic> json) => SplashData(
      content: json['content'] as String?,
    );
