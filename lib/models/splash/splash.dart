import 'package:json_annotation/json_annotation.dart';

part 'splash.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Splash {
  String? title;
  // List<dynamic>? data;
  Splash({
    this.title,
    // this.data,
  });
  factory Splash.fromJson(Map<String, dynamic> json) => _$SplashFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class SplashData {
  String? content;
  SplashData({
    this.content,
  });
  factory SplashData.fromJson(Map<String, dynamic> json) =>
      _$SplashDataFromJson(json);
}
