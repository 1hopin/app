import 'package:json_annotation/json_annotation.dart';

part 'customer_otp_request.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class CustomerOtpRequest {
  OtpData? otp;
  TypeData? type;

  CustomerOtpRequest({
    this.otp,
    this.type,
  });
  factory CustomerOtpRequest.fromJson(Map<String, dynamic> json) =>
      _$CustomerOtpRequestFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class OtpData {
  String? refno;
  String? status;
  String? token;

  OtpData({
    this.refno,
    this.status,
    this.token,
  });
  factory OtpData.fromJson(Map<String, dynamic> json) =>
      _$OtpDataFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class TypeData {
  String? isType;

  TypeData({
    this.isType,
  });
  factory TypeData.fromJson(Map<String, dynamic> json) =>
      _$TypeDataFromJson(json);
}
