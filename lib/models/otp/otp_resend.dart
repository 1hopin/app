import 'package:json_annotation/json_annotation.dart';

part 'otp_resend.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class OtpResend {
  OtpData? otp;

  OtpResend({
    this.otp,
  });
  factory OtpResend.fromJson(Map<String, dynamic> json) =>
      _$OtpResendFromJson(json);
}

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class OtpData {
  String? refno;
  String? status;
  String? token;

  OtpData({
    this.refno,
    this.status,
    this.token,
  });
  factory OtpData.fromJson(Map<String, dynamic> json) =>
      _$OtpDataFromJson(json);
}
