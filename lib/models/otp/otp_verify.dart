import 'package:json_annotation/json_annotation.dart';

part 'otp_verify.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class OtpVerify {
  String? detail;
  String? message;
  String? status;

  OtpVerify({
    this.detail,
    this.message,
    this.status,
  });
  factory OtpVerify.fromJson(Map<String, dynamic> json) =>
      _$OtpVerifyFromJson(json);
}
