import 'package:json_annotation/json_annotation.dart';

part 'customers_profile.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class CustomersProfile {
  int id;
  String? firstname;
  String? lastname;
  String? phoneNo;

  CustomersProfile({
    required this.id,
    this.firstname,
    this.lastname,
    this.phoneNo,
  });
  factory CustomersProfile.fromJson(Map<String, dynamic> json) =>
      _$CustomersProfileFromJson(json);
}
