// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_generic_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseGenericData _$ResponseGenericDataFromJson(Map<String, dynamic> json) =>
    ResponseGenericData(
      code: json['code'] as String?,
      data: json['data'],
    );
