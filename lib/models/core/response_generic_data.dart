import 'package:json_annotation/json_annotation.dart';

part 'response_generic_data.g.dart';

@JsonSerializable(createToJson: false)
class ResponseGenericData {
  String? code;
  dynamic data;

  ResponseGenericData({required this.code, required this.data});

  factory ResponseGenericData.fromJson(Map<String, dynamic> json) =>
      _$ResponseGenericDataFromJson(json);
}
