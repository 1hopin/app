import 'package:json_annotation/json_annotation.dart';

part 'signup.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Signup {
  String? token;

  Signup({
    this.token,
  });
  factory Signup.fromJson(Map<String, dynamic> json) => _$SignupFromJson(json);
}
