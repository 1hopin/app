class Resource<T> {
  final Status status;
  final String? code;
  final T? data;
  final String? message;

  Resource({required this.status, this.code, this.data, this.message});

  static Resource<T> success<T>({required T data}) {
    return Resource<T>(status: Status.SUCCESS, data: data);
  }

  static Resource<T> error<T>(
      {required String code, required String error, T? data}) {
    return Resource<T>(
        status: Status.ERROR, code: code, message: error, data: data);
  }

  static Resource<T> loading<T>() {
    return Resource<T>(status: Status.LOADING);
  }
}

class StatusCode {
  static const int NO_ERROR = 0;
  static const NETWORK_ERROR = 1;
  static const GENERAL_ERROR = 2;
}

enum Status { SUCCESS, ERROR, LOADING }
