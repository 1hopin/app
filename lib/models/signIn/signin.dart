import 'package:json_annotation/json_annotation.dart';

part 'signin.g.dart';

@JsonSerializable(createToJson: false, fieldRename: FieldRename.snake)
class Signin {
  String? token;

  Signin({
    this.token,
  });
  factory Signin.fromJson(Map<String, dynamic> json) => _$SigninFromJson(json);
}
