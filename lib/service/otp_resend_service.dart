import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/otp/otp_resend.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/interfaces/otp_resend_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class OtpResendService implements OtpResendApi {
  late Dio dio;
  late ApiManager apiManager;

  OtpResendService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<OtpResend?>>? getOtpResend(String? phone) async {
    var body = {
      'phone_no': phone,
    };
    try {
      final result = await apiManager.getOtpResend(body);
      return Resource.success(data: OtpResend.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
