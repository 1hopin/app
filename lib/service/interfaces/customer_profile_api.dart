import 'package:onehopin/models/customersProfile/customers_profile.dart';
import 'package:onehopin/models/resource.dart';

abstract class CustomerProfileApi {
  Future<Resource<CustomersProfile?>>? getProfileCustomer();
}
