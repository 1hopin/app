import 'package:onehopin/models/signUp/signup.dart';

import '../../models/resource.dart';

abstract class SignupApi {
  Future<Resource<Signup?>>? signupCustomer(
      String? phone, String? name, String? lastname);
}
