import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/splash/splash.dart';

abstract class SplashApi {
  Future<Resource<Splash?>>? getSplashConfig();
}
