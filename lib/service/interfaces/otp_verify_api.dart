import 'package:onehopin/models/otp/otp_verify.dart';
import 'package:onehopin/models/resource.dart';

abstract class OtpVerifyApi {
  Future<Resource<OtpVerify?>>? checkOtpVerify(String? token, String? pin);
}
