import 'package:onehopin/models/otp/customer_otp_request.dart';
import 'package:onehopin/models/resource.dart';

abstract class CustomerOtpRequestApi {
  Future<Resource<CustomerOtpRequest?>>? getCustomerOtp(String? phoneno);
}
