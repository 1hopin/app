import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/signIn/signin.dart';

abstract class SigninApi {
  Future<Resource<Signin?>>? signInCustomer(
      String? token, String? pin, String? phoneno);
}
