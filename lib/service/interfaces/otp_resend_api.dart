import 'package:onehopin/models/otp/otp_resend.dart';

import '../../models/resource.dart';

abstract class OtpResendApi {
  Future<Resource<OtpResend?>>? getOtpResend(String? phone);
}
