import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/signUp/signup.dart';
import 'package:onehopin/service/interfaces/signup_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class SignupService implements SignupApi {
  late Dio dio;
  late ApiManager apiManager;

  SignupService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<Signup?>>? signupCustomer(
      String? phone, String? name, String? lastname) async {
    var body = {
      'phone_no': phone,
      'firstname': name,
      'lastname': lastname,
    };
    try {
      final result = await apiManager.signupCustomer(body);
      return Resource.success(data: Signup.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
