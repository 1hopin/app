import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/otp/otp_verify.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/interfaces/otp_verify_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class OtpVerifyService implements OtpVerifyApi {
  late Dio dio;
  late ApiManager apiManager;

  OtpVerifyService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<OtpVerify?>>? checkOtpVerify(
      String? token, String? pin) async {
    var body = {
      'token': token,
      'pin': pin,
    };
    try {
      final result = await apiManager.checkOtpVerify(body);
      return Resource.success(data: OtpVerify.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
