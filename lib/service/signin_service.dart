import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/models/signIn/signin.dart';
import 'package:onehopin/service/interfaces/signin_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class SigninService implements SigninApi {
  late Dio dio;
  late ApiManager apiManager;

  SigninService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<Signin?>>? signInCustomer(
      String? token, String? pin, String? phoneno) async {
    var body = {
      'token': token,
      'pin': pin,
      'phone_no': phoneno,
    };
    try {
      final result = await apiManager.signInCustomer(body);
      return Resource.success(data: Signin.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
