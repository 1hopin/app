import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/otp/customer_otp_request.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/interfaces/customer_otp_request_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class CustomerOtpRequestService implements CustomerOtpRequestApi {
  late Dio dio;
  late ApiManager apiManager;

  CustomerOtpRequestService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<CustomerOtpRequest?>>? getCustomerOtp(String? phoneno) async {
    var body = {'phone_no': phoneno};
    try {
      final result = await apiManager.getCustomerOtpsRequest(body);
      return Resource.success(data: CustomerOtpRequest.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
