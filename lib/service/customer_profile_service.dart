import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:onehopin/models/customersProfile/customers_profile.dart';
import 'package:onehopin/models/resource.dart';
import 'package:onehopin/service/interfaces/customer_profile_api.dart';
import 'package:onehopin/utils/api_manager.dart';

@singleton
class CustomerProfileService implements CustomerProfileApi {
  late Dio dio;
  late ApiManager apiManager;

  CustomerProfileService() {
    dio = Dio();
    apiManager = ApiManager(dio);
  }

  @override
  Future<Resource<CustomersProfile?>>? getProfileCustomer() async {
    try {
      final result = await apiManager.getCustomerProfile();
      return Resource.success(data: CustomersProfile.fromJson(result.data));
    } catch (e) {
      if (e is DioError) {
        final res = e.response!.data;
        return Resource.error(code: res["code"], error: res['message']);
      }
      return Resource.error(code: "unknown_error", error: "unknown_error");
    }
  }
}
