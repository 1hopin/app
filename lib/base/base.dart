import 'package:onehopin/constants/app_key.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Base {
  Future<String> getDeviceId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(deviceId) ?? "";
  }

  Future<String> getUserLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('locale') ?? 'th';
  }

  static String? _appName;
  static String? _packageName;
  static String? _version;
  static String? _buildNumber;

  static void setPackageInfo({appName, packageName, version, buildNumber}) {
    _appName = appName;
    _packageName = packageName;
    _version = version;
    _buildNumber = buildNumber;
  }

  static PackageInfo getPackageInfo() {
    return PackageInfo(
        appName: _appName!,
        packageName: _packageName!,
        version: _version!,
        buildNumber: _buildNumber!);
  }

  Future<String?> getTokenHeader() async {
    String? result;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString(tokenCustomer) != null) {
      result = prefs.getString(tokenCustomer);
    }
    return result;
  }
}
