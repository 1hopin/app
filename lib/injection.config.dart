// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'service/customer_otp_request_service.dart' as _i3;
import 'service/customer_profile_service.dart' as _i4;
import 'service/otp_resend_service.dart' as _i9;
import 'service/otp_verify_service.dart' as _i10;
import 'service/signin_service.dart' as _i16;
import 'service/signup_service.dart' as _i17;
import 'service/splash_service.dart' as _i18;
import 'ui/home/home_viewmodel.dart' as _i5;
import 'ui/invalidAccount/invalid_account_viewmodel.dart' as _i6;
import 'ui/main/main_viewmodel.dart' as _i8;
import 'ui/otp/otp_viewmodel.dart' as _i11;
import 'ui/requestOTP/request_otp_viewmodel.dart' as _i12;
import 'ui/signIn/sign_in_viewmodel.dart' as _i13;
import 'ui/signUp/sign_up_viewmodel.dart' as _i15;
import 'ui/signUpSuccess/sign_up_success_viewmodel.dart' as _i14;
import 'ui/verifying/verifying_viewmodel.dart' as _i19;
import 'utils/localization_util.dart'
    as _i7; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.singleton<_i3.CustomerOtpRequestService>(_i3.CustomerOtpRequestService());
  gh.singleton<_i4.CustomerProfileService>(_i4.CustomerProfileService());
  gh.factory<_i5.HomeViewModel>(() => _i5.HomeViewModel());
  gh.factory<_i6.InvalidAccountViewmodel>(() => _i6.InvalidAccountViewmodel());
  gh.factory<_i7.LocalizationUtil>(() => _i7.LocalizationUtil());
  gh.factory<_i8.MainViewModel>(() => _i8.MainViewModel());
  gh.singleton<_i9.OtpResendService>(_i9.OtpResendService());
  gh.singleton<_i10.OtpVerifyService>(_i10.OtpVerifyService());
  gh.factory<_i11.OtpViewModel>(() => _i11.OtpViewModel());
  gh.factory<_i12.RequestOtpViewmodel>(() => _i12.RequestOtpViewmodel());
  gh.factory<_i13.SignInViewModel>(() => _i13.SignInViewModel());
  gh.factory<_i14.SignUpSuccessViewModel>(() => _i14.SignUpSuccessViewModel());
  gh.factory<_i15.SignUpViewModel>(() => _i15.SignUpViewModel());
  gh.singleton<_i16.SigninService>(_i16.SigninService());
  gh.singleton<_i17.SignupService>(_i17.SignupService());
  gh.singleton<_i18.SplashService>(_i18.SplashService());
  gh.factory<_i19.VerifyingViewModel>(() => _i19.VerifyingViewModel());
  return get;
}
